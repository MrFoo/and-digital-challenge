package uk.foocode.anddigitalchallenge.viewmodel

import android.text.Html
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import uk.foocode.anddigitalchallenge.data.ChuckNorrisWebService
import uk.foocode.anddigitalchallenge.data.IChuckNorrisWebService
import uk.foocode.anddigitalchallenge.data.model.Joke
import java.net.URLDecoder
import java.net.URLEncoder
import kotlin.random.Random

interface IJokeViewModel {
    val uiState: StateFlow<JokeListState>
    val singleJokeState: StateFlow<JokeListState>
    val textInputState: StateFlow<TextInputJokeState>
    val explicitToggleState: StateFlow<Boolean>
    fun getSingleJoke()
    fun closeSingleJoke()
    fun askForTextInput()
    fun getNamedJoke(name: String)
    fun closeNamedJoke()
    fun showJokeList()
    fun loadMoreJokes()
    fun closeJokeList()
    fun toggleExplicit()
}

sealed interface JokeListState{
    object Empty : JokeListState
    class Loaded(val data: List<Joke>) : JokeListState
    class Error(val existingData:List<Joke>,val message: String) : JokeListState
}

sealed interface TextInputJokeState{
    object Empty : TextInputJokeState
    object EnterInput : TextInputJokeState
    object InputError: TextInputJokeState
    object Loading : TextInputJokeState
    class Loaded(val data: Joke) : TextInputJokeState
    class Error(val message : String) : TextInputJokeState
}

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 01/06/2022.
 */
class JokeViewModel(private val webService: IChuckNorrisWebService = ChuckNorrisWebService()) : ViewModel(),
    IJokeViewModel {

    private val _jokeState = MutableStateFlow<JokeListState>(JokeListState.Empty)
    override val uiState: StateFlow<JokeListState> = _jokeState

    private val _singleJokeState = MutableStateFlow<JokeListState>(JokeListState.Empty)
    override val singleJokeState:StateFlow<JokeListState> = _singleJokeState

    private val _textInputState = MutableStateFlow<TextInputJokeState>(TextInputJokeState.Empty)
    override val textInputState:StateFlow<TextInputJokeState> = _textInputState

    private val _explicitToggleState = MutableStateFlow(false)
    override val explicitToggleState: StateFlow<Boolean> = _explicitToggleState

    override fun getSingleJoke(){
        viewModelScope.launch(Dispatchers.IO) {
            val response=webService.getSingleJoke(_explicitToggleState.value)
            response.onSuccess {
                _singleJokeState.emit(JokeListState.Loaded(listOf(cleanHtmlEntities(it.value))))
            }
            response.onFailure {
                _singleJokeState.emit(JokeListState.Error(listOf(),it.message ?: "Unknown error"))
            }
        }
    }
    override fun closeSingleJoke(){
        viewModelScope.launch {
            _singleJokeState.emit(JokeListState.Empty)
        }
    }

    override fun askForTextInput(){
        viewModelScope.launch {
            _textInputState.emit(TextInputJokeState.EnterInput)
        }
    }

    override fun getNamedJoke(name:String){
        val firstName=name.split(" ").firstOrNull() ?: ""
        val lastName=name.split(" ").drop(1).joinToString(" ")

        viewModelScope.launch {
            if (firstName.isBlank() || lastName.isBlank()) {
                _textInputState.emit(TextInputJokeState.InputError)
            } else {
                _textInputState.emit(TextInputJokeState.Loading)
                val response = webService.getNamedJoke(firstName,lastName, _explicitToggleState.value)
                response.onSuccess {
                    _textInputState.emit(TextInputJokeState.Loaded(cleanHtmlEntities(it.value)))
                }
                response.onFailure {
                    _textInputState.emit(TextInputJokeState.Error(it.message ?: "Unknown error"))
                }
            }
        }
    }

    override fun closeNamedJoke(){
        viewModelScope.launch {
            _textInputState.emit(TextInputJokeState.Empty)
        }
    }

    private val loadedJokes = mutableListOf<Joke>()

    override fun showJokeList(){
        viewModelScope.launch {
            loadedJokes.clear()
            _jokeState.emit(JokeListState.Loaded(listOf(Joke(-1,"LOADING", listOf()))))
        }
        loadJokes()
    }

    override fun loadMoreJokes() {
        viewModelScope.launch {
            _jokeState.emit(JokeListState.Loaded(loadedJokes + Joke(1,"LOADING", listOf())))
        }
        loadJokes()

    }

    private fun loadJokes(){
        viewModelScope.launch {
            val response = webService.getRandomJokes(20,_explicitToggleState.value)
            response.onSuccess {
                loadedJokes.addAll(it.value?.map { joke ->
                    cleanHtmlEntities(joke)
                } ?: listOf())
                _jokeState.emit(JokeListState.Loaded(loadedJokes))
            }
            response.onFailure {
                _jokeState.emit(JokeListState.Error(loadedJokes,it.message ?: "Unknown error"))
            }
        }
    }

    override fun closeJokeList() {
        viewModelScope.launch {
            _jokeState.emit(JokeListState.Empty)
        }
    }

    override fun toggleExplicit() {
        viewModelScope.launch {
            _explicitToggleState.emit(!_explicitToggleState.value)
        }
    }


    private fun cleanHtmlEntities(joke:Joke):Joke{

        return try {
            Joke(
                joke.id,
                Html.fromHtml(joke.joke,Html.FROM_HTML_MODE_LEGACY).toString(),
                joke.categories
            )
        } catch(e:Exception){
            joke
        }
    }

}


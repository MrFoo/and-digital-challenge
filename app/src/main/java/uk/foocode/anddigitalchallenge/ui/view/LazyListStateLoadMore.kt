package uk.foocode.anddigitalchallenge.ui.view

import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.runtime.*

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 15/06/2022.
 */
@Composable
fun LazyListState.OnBottomReached(
    buffer : Int = 5,
    onLoadMore : () -> Unit
) {
    require(buffer >= 0) { "buffer cannot be negative, but was $buffer" }

    val shouldLoadMore = remember {
        derivedStateOf {
            val lastVisibleItem = layoutInfo.visibleItemsInfo.lastOrNull()
                ?:
                return@derivedStateOf true

            lastVisibleItem.index >=  layoutInfo.totalItemsCount - 1 - buffer
        }
    }

    LaunchedEffect(shouldLoadMore){
        snapshotFlow { shouldLoadMore.value }
            .collect { if (it) onLoadMore() }
    }
}
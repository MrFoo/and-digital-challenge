package uk.foocode.anddigitalchallenge.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
val Red300 = Color(0xFFE57373)
val Blue50 = Color(0xFFE3F2FD)
val Green50 = Color(0xFFE8F5E9)
val FadeBackground = Color(0xAA000000)
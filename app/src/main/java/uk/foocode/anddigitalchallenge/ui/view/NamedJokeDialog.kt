package uk.foocode.anddigitalchallenge.ui.view

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import androidx.lifecycle.viewmodel.compose.viewModel
import uk.foocode.anddigitalchallenge.R
import uk.foocode.anddigitalchallenge.viewmodel.JokeViewModel
import uk.foocode.anddigitalchallenge.viewmodel.TextInputJokeState


/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 15/06/2022.
 */
@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun NamedJokeDialog() {
    val jokeViewModel:JokeViewModel = viewModel()
    val state by jokeViewModel.textInputState.collectAsState()

    when{
        (state is TextInputJokeState.EnterInput ) || (state is TextInputJokeState.InputError) -> Dialog(
            onDismissRequest = { jokeViewModel.closeNamedJoke() },
            properties = DialogProperties(usePlatformDefaultWidth = false)
        ) {
            var name by rememberSaveable {
                mutableStateOf("")
            }
            val errorState = state is TextInputJokeState.InputError
            Column(modifier = Modifier
                .background(color = Color.White, shape = RoundedCornerShape(8.dp))
                .padding(16.dp)) {
                TextField(
                    value = name,
                    onValueChange = { name=it },
                    isError = errorState,
                    label = {
                        Text(stringResource(R.string.name_input_label))
                    },
                    singleLine = true,
                    keyboardOptions = KeyboardOptions(capitalization = KeyboardCapitalization.Words))
                if(errorState){
                    Text("Please enter both a first and last name", style = MaterialTheme.typography.subtitle1)
                }
                Button(onClick = {
                    jokeViewModel.getNamedJoke(name)
                }) {
                    Text(stringResource(id = R.string.submit_btn))
                }

            }
        }
        state  is TextInputJokeState.Loading -> Dialog(onDismissRequest = { jokeViewModel.closeNamedJoke() }) {
            Column(modifier = Modifier
                .background(color = Color.White, shape = RoundedCornerShape(8.dp))
                .padding(16.dp)) {
                Text(stringResource(R.string.loading_message))
            }
        }
        state is TextInputJokeState.Loaded -> Dialog(onDismissRequest = { jokeViewModel.closeNamedJoke() }) {
            Column(modifier = Modifier
                .background(color = Color.White, shape = RoundedCornerShape(8.dp))
                .padding(16.dp)) {
                //state could change so need local static copy.
                val currentState: TextInputJokeState = state
                if(currentState is TextInputJokeState.Loaded) {
                    Text(currentState.data.joke)
                }
                Button(onClick = { jokeViewModel.closeNamedJoke() },modifier = Modifier.padding(top=8.dp)){
                    Text(stringResource(R.string.ok_message))
                }
            }
        }
        else -> {}
    }
}
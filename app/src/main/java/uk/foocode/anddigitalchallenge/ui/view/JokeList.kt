package uk.foocode.anddigitalchallenge.ui.view

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import uk.foocode.anddigitalchallenge.R
import uk.foocode.anddigitalchallenge.ui.theme.Blue50
import uk.foocode.anddigitalchallenge.ui.theme.FadeBackground
import uk.foocode.anddigitalchallenge.ui.theme.Green50
import uk.foocode.anddigitalchallenge.viewmodel.JokeListState
import uk.foocode.anddigitalchallenge.viewmodel.JokeViewModel

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 01/06/2022.
 */
@Composable
fun JokeList(modifier:Modifier=Modifier, jokeViewModel: JokeViewModel = viewModel() ){
    val state by jokeViewModel.uiState.collectAsState()
    if(state is JokeListState.Loaded || state is JokeListState.Error) {
        Column(
            modifier = modifier
                .background(color = FadeBackground)
                .clickable { jokeViewModel.closeJokeList() }
                .padding(horizontal = 32.dp, vertical = 48.dp)
                .clickable(enabled = false) {}
                .background(color = Color.White, shape = RoundedCornerShape(8.dp))
                .padding(16.dp)
        ) {
            val listState = rememberLazyListState()
            listState.OnBottomReached {
                jokeViewModel.loadMoreJokes()
            }
            LazyColumn(state = listState, modifier = Modifier.weight(1f)) {
                when (state) {
                    is JokeListState.Error -> {
                        val items = (state as JokeListState.Error)
                        itemsIndexed(items = items.existingData) { index, item ->
                            val backgroundColor = when (index % 2) {
                                0 -> Blue50
                                else -> Green50
                            }
                            Log.d("CAT","${item.categories}")
                            JokeView(
                                joke = item, modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(vertical = 8.dp)
                                    .background(
                                        color = backgroundColor,
                                        shape = RoundedCornerShape(8.dp)
                                    )
                            )

                        }
                        item {
                            ErrorMessage(
                                message = stringResource(R.string.joke_load_error),
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(vertical = 8.dp)
                                    .clickable {
                                        jokeViewModel.loadMoreJokes()
                                    }
                            )
                        }
                    }
                    is JokeListState.Loaded -> itemsIndexed(items = (state as JokeListState.Loaded).data) { index, item ->
                        val backgroundColor = when (index % 2) {
                            0 -> Blue50
                            else -> Green50
                        }
                        JokeView(
                            joke = item, modifier = Modifier
                                .fillMaxWidth()
                                .padding(vertical = 8.dp)
                                .background(
                                    color = backgroundColor,
                                    shape = RoundedCornerShape(8.dp)
                                )
                        )

                    }
                    else -> {}
                }
            }
            Button(onClick = { jokeViewModel.closeJokeList() }) {
                Text(stringResource(R.string.close_btn))
            }

        }
    }

}




package uk.foocode.anddigitalchallenge.ui.view

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import uk.foocode.anddigitalchallenge.ui.theme.ANDDigitalTheme

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 01/06/2022.
 */
@Composable
fun ErrorMessage(message:String, modifier:Modifier=Modifier) {
    Card(modifier, backgroundColor = MaterialTheme.colors.error) {
        Text(message,
            style = MaterialTheme.typography.body2,
            textAlign = TextAlign.Center,
            modifier = Modifier.padding(16.dp))
    }
}

@Preview
@Composable
fun TestError(){
    ANDDigitalTheme {
        ErrorMessage(message = "Unable to load data",modifier=Modifier.fillMaxWidth())
    }
}
package uk.foocode.anddigitalchallenge.ui.view

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import uk.foocode.anddigitalchallenge.viewmodel.JokeViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import uk.foocode.anddigitalchallenge.R
import uk.foocode.anddigitalchallenge.viewmodel.JokeListState

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 14/06/2022.
 */
@Composable
fun JokeDialog(){
    val jokeViewModel: JokeViewModel = viewModel()
    val singleJokeState by jokeViewModel.singleJokeState.collectAsState()
    if(singleJokeState is JokeListState.Loaded){
        val jokeState=(singleJokeState as JokeListState.Loaded)
        Dialog(onDismissRequest = { jokeViewModel.closeSingleJoke() }) {
            Column(modifier = Modifier
                .background(color = Color.White, shape = RoundedCornerShape(8.dp))
                .padding(16.dp)) {
                Text(jokeState.data.first().joke)
                Button(onClick = { jokeViewModel.closeSingleJoke() },modifier = Modifier.padding(top=8.dp)) {
                    Text(stringResource(id = R.string.ok_message))
                }
            }
        }
    } else if(singleJokeState is JokeListState.Error){
        Dialog(onDismissRequest = { jokeViewModel.closeSingleJoke() }) {
            Column(modifier = Modifier
                .background(color = MaterialTheme.colors.error, shape = RoundedCornerShape(8.dp))
                .padding(16.dp)) {
                Text("Unable to load joke")
                Button(onClick = { jokeViewModel.closeSingleJoke() },modifier = Modifier.padding(top=8.dp)) {
                    Text(stringResource(id = R.string.ok_message))
                }
            }
        }
    }
}
package uk.foocode.anddigitalchallenge.ui.view

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import uk.foocode.anddigitalchallenge.data.model.Joke

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 01/06/2022.
 */

@Composable
fun JokeView(joke:Joke, modifier:Modifier = Modifier){
    Box(modifier) {
        Text(joke.joke,modifier=Modifier.padding(16.dp))
    }
}

@Preview
@Composable
fun TestJokeView(){
    JokeView(joke = Joke(2,"Test joke text", listOf()))
}

package uk.foocode.anddigitalchallenge.ui.view

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import uk.foocode.anddigitalchallenge.R
import uk.foocode.anddigitalchallenge.ui.theme.ANDDigitalTheme

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 01/06/2022.
 */

@Composable
fun Loading(modifier:Modifier=Modifier){
    Card(modifier){
        Text(
            stringResource(id = R.string.loading_message),
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth(),
            textAlign = TextAlign.Center,
            style = MaterialTheme.typography.body2)
    }
}

@Preview
@Composable
fun TestLoading(){
    ANDDigitalTheme {
        Loading()
    }
}
package uk.foocode.anddigitalchallenge.data

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import uk.foocode.anddigitalchallenge.data.model.JokeListResponse
import uk.foocode.anddigitalchallenge.data.model.SingleJokeResponse

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 31/05/2022.
 */
interface ChuckNorrisApi {

    @GET("random")
    suspend fun getSingleJoke(@Query("firstName") firstName:String="Chuck", @Query("lastName") lastName:String="Norris", @Query("exclude") exclusionList:String="[]") : Response<SingleJokeResponse>

    @GET("random/{count}")
    suspend fun getMultipleJokes(@Path("count") count:Int,@Query("firstName") firstName:String="Chuck", @Query("lastName") lastName:String="Norris",@Query("exclude") exclusionList:String="[]") : Response<JokeListResponse>
}
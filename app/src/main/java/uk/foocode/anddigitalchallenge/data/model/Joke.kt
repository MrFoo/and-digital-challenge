package uk.foocode.anddigitalchallenge.data.model

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 31/05/2022.
 */
data class Joke(val id:Int,val joke:String,val categories:List<String>)

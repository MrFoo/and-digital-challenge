package uk.foocode.anddigitalchallenge.data

import androidx.annotation.IntRange
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import uk.foocode.anddigitalchallenge.BuildConfig
import uk.foocode.anddigitalchallenge.data.model.*


/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 31/05/2022.
 */
interface IChuckNorrisWebService {
    suspend fun getRandomJokes(count:Int,excludeExplicit:Boolean=false):Result<JokeListResponse>
    suspend fun getSingleJoke(excludeExplicit:Boolean=false):Result<SingleJokeResponse>
    suspend fun getNamedJoke(firstName:String,lastName:String,excludeExplicit:Boolean=false):Result<SingleJokeResponse>
}

class ChuckNorrisWebService(
    private var api:ChuckNorrisApi = Retrofit.Builder()
        .baseUrl(BuildConfig.API_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(ChuckNorrisApi::class.java)) : IChuckNorrisWebService {
    override suspend fun getRandomJokes(count:Int,excludeExplicit:Boolean) = safeCall {
        api.getMultipleJokes(count, exclusionList = genExcludeString(excludeExplicit))
    }
    override suspend fun getSingleJoke(excludeExplicit:Boolean) = safeCall {
        api.getSingleJoke(exclusionList = genExcludeString(excludeExplicit))
    }
    override suspend fun getNamedJoke(firstName: String,lastName: String,excludeExplicit:Boolean): Result<SingleJokeResponse> = safeCall {
        api.getSingleJoke(firstName,lastName,exclusionList = genExcludeString(excludeExplicit))
    }

    private fun genExcludeString(exclude:Boolean):String = when(exclude){
        true -> "[explicit]"
        else -> "[]"
    }
}
package uk.foocode.anddigitalchallenge.data.model

import retrofit2.Response
import kotlin.Exception

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 01/06/2022.
 */
suspend fun <T:Any> safeCall(method:suspend ()-> Response<T>) : Result<T> {
    return try {
        val result = method()
        if (result.isSuccessful && result.body() != null) {
            Result.success(result.body()!!)
        } else {
            when(result.code()){
                500 -> Result.failure(ApiException("Server error"))
                401 -> Result.failure(ApiException("Invalid username/password"))
                404 -> Result.failure(ApiException("Not found"))
                else -> Result.failure(ApiException("Unknown error ${result.code()}"))
            }
        }
    }catch(e: Exception){
        Result.failure(e)
    }
}

class ApiException(message:String) : Exception(message)
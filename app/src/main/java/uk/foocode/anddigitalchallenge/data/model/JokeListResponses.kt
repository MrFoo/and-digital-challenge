package uk.foocode.anddigitalchallenge.data.model

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 31/05/2022.
 *
 */

data class JokeListResponse( val type:String, val value:List<Joke>?)

data class SingleJokeResponse( val type:String, val value:Joke)

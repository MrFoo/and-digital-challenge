package uk.foocode.anddigitalchallenge

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import uk.foocode.anddigitalchallenge.ui.theme.ANDDigitalTheme
import uk.foocode.anddigitalchallenge.ui.view.JokeDialog
import uk.foocode.anddigitalchallenge.ui.view.JokeList
import uk.foocode.anddigitalchallenge.ui.view.NamedJokeDialog
import uk.foocode.anddigitalchallenge.viewmodel.JokeViewModel

class MainActivity : ComponentActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            ANDDigitalTheme {
                Box() {
                    Column(modifier = Modifier.fillMaxSize()) {
                        val jokeViewModel: JokeViewModel = viewModel()

                        TopAppBar(title = { Text(stringResource(id = R.string.app_name)) })
                        JokeDialog()
                        NamedJokeDialog()
                        Button(
                            onClick = {
                                jokeViewModel.getSingleJoke()
                            },
                            contentPadding = PaddingValues(vertical = 16.dp),
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(horizontal = 16.dp, vertical = 8.dp)) {
                            Text(getString(R.string.random_btn))
                        }
                        Button(onClick = {
                                jokeViewModel.askForTextInput()
                            },
                            contentPadding = PaddingValues(vertical = 16.dp),
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(horizontal = 16.dp, vertical = 8.dp)) {
                            Text(getString(R.string.text_input_btn))
                        }
                        Button(
                            onClick = {
                                jokeViewModel.showJokeList()
                            },
                            contentPadding = PaddingValues(vertical = 16.dp),
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(horizontal = 16.dp, vertical = 8.dp)) {
                            Text(getString(R.string.infinite_btn))
                        }


                        Row(
                            verticalAlignment = Alignment.CenterVertically,
                            modifier = Modifier.clickable { jokeViewModel.toggleExplicit() }) {
                            Checkbox(
                                checked = jokeViewModel.explicitToggleState.collectAsState(false).value,
                                onCheckedChange = { jokeViewModel.toggleExplicit() })
                            Text("Hide Explicit Jokes")
                        }
                    }
                    JokeList(modifier = Modifier.fillMaxSize())
                }
            }
        }
    }
}

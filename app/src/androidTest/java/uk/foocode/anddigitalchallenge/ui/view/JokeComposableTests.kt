package uk.foocode.anddigitalchallenge.ui.view

import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import uk.foocode.anddigitalchallenge.data.IChuckNorrisWebService
import uk.foocode.anddigitalchallenge.data.model.*
import uk.foocode.anddigitalchallenge.viewmodel.JokeViewModel

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 01/06/2022.
 */
@RunWith(AndroidJUnit4::class)
class JokeComposableTests {

    private lateinit var mockViewModel: JokeViewModel
    private lateinit var temporaryErrorViewModel : JokeViewModel
    private val goodResponse= JokeListResponse("success", listOf(
        Joke(1,"Joke body 1",listOf("a","b")),
        Joke(2,"Joke body &quot;2&quot;", listOf("b","c")),
        Joke(3,"Joke body 1",listOf("a","b")),
        Joke(4,"Joke body &quot;2&quot;", listOf("b","c")),
        Joke(5,"Joke body 1",listOf("a","b")),
        Joke(6,"Joke body &quot;2&quot;", listOf("b","c")),
        Joke(7,"Joke body 1",listOf("a","b")),
        Joke(8,"Joke body &quot;2&quot;", listOf("b","c")),
    ))
    private val goodJoke = Joke(1,"Joke body 1",listOf("a","b"))
    private val temporaryErrorText = "Please try again"
    @Before
    fun setUp(){
        mockViewModel= JokeViewModel(
            object : IChuckNorrisWebService{
                override suspend fun getRandomJokes(
                    count: Int,
                    excludeExplicit: Boolean
                ): Result<JokeListResponse> {
                    return Result.success(goodResponse)
                }

                override suspend fun getSingleJoke(excludeExplicit: Boolean): Result<SingleJokeResponse> {
                    return Result.success(SingleJokeResponse("",goodJoke))
                }

                override suspend fun getNamedJoke(
                    firstName: String,
                    lastName: String,
                    excludeExplicit: Boolean
                ): Result<SingleJokeResponse> {
                    return Result.success(SingleJokeResponse("",Joke(1,"good $firstName joke $lastName", listOf())))
                }
            }
        )
        temporaryErrorViewModel = JokeViewModel(
            object : IChuckNorrisWebService{
                override suspend fun getRandomJokes(
                    count: Int,
                    excludeExplicit: Boolean
                ): Result<JokeListResponse> {
                    return Result.success(goodResponse)
                }

                override suspend fun getSingleJoke(excludeExplicit: Boolean): Result<SingleJokeResponse> {
                    return Result.success(SingleJokeResponse("",goodJoke))
                }

                override suspend fun getNamedJoke(
                    firstName: String,
                    lastName: String,
                    excludeExplicit: Boolean
                ): Result<SingleJokeResponse> {
                    return Result.success(SingleJokeResponse("",Joke(1,"good $firstName joke $lastName", listOf())))
                }
            }
        )

    }

    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun containsAtLeast3Entries():Unit = runBlocking {
        composeTestRule.setContent {
            JokeList(jokeViewModel = mockViewModel)
        }
        delay(1000)
        composeTestRule.onNode(hasScrollAction()).onChildAt(0).assertExists("Should have at least 1 child")
        composeTestRule.onNode(hasScrollAction()).onChildAt(0).assert(hasText(goodResponse.value!![0].joke))
        composeTestRule.onNode(hasScrollAction()).onChildAt(3).assertExists("Should have at least 3 children")
    }

    @Test
    fun showsTemporaryError():Unit = runBlocking {
        composeTestRule.setContent {
            JokeList(jokeViewModel = temporaryErrorViewModel)
        }
        delay(100)
        composeTestRule.onNode(hasText(temporaryErrorText)).assertExists()
    }

    @Test
    fun errorIsClickable():Unit = runBlocking {
        composeTestRule.setContent {
            JokeList(jokeViewModel = temporaryErrorViewModel)
        }
        delay(100)
        composeTestRule.onNode(hasText("try again",substring = true)).assertHasClickAction()
    }

    @Test
    fun jokeComposableShowsText(): Unit = runBlocking {
        composeTestRule.setContent {
            JokeView(joke = goodJoke)
        }
        composeTestRule.onRoot().onChild().assert(hasText(goodJoke.joke))
    }
}
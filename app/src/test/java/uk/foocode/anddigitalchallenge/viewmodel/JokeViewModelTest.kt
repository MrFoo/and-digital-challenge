package uk.foocode.anddigitalchallenge.viewmodel

import io.mockk.coEvery
import io.mockk.mockkClass
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import uk.foocode.anddigitalchallenge.MainCoroutineRule
import uk.foocode.anddigitalchallenge.data.ChuckNorrisWebService
import uk.foocode.anddigitalchallenge.data.model.ApiException
import uk.foocode.anddigitalchallenge.data.model.Joke
import uk.foocode.anddigitalchallenge.data.model.JokeListResponse
import uk.foocode.anddigitalchallenge.data.model.SingleJokeResponse

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 01/06/2022.
 */
class JokeViewModelTest {

    private lateinit var viewModel: JokeViewModel
    private val mockWebService = mockkClass(ChuckNorrisWebService::class)
    private val goodResponse=JokeListResponse("success", listOf(
        Joke(1,"Joke body 1",listOf("a","b")),
        Joke(2,"Joke body &quot;2&quot;", listOf("b","c"))
    ))
    private val goodJoke=Joke(1,"Joke body 1",listOf("a","b"))


    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    val coroutineRule = MainCoroutineRule()

    @Before
    fun setUp(){
        viewModel= JokeViewModel(mockWebService)
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `List state returns data`() = runBlocking {

        coEvery { mockWebService.getRandomJokes(any()) } returns Result.success(goodResponse)

        val results = mutableListOf<JokeListState>()
        viewModel.uiState.onEach { results.add(it) }.launchIn(CoroutineScope(
            UnconfinedTestDispatcher()))

        viewModel.showJokeList()
        delay(200)
        assert(results[0] is JokeListState.Empty) { "Should start in empty state" }
        assert(results[1] is JokeListState.Loaded) { "Should progress to loaded with text LOADING" }
        assert((results[1] as JokeListState.Loaded).data.first().joke.contains("LOADING"))
        assert(results[2] is JokeListState.Loaded)

        val state=viewModel.uiState.value
        assert(state is JokeListState.Loaded)
        state as JokeListState.Loaded
        assert(state.data.size == 2)

        viewModel.loadMoreJokes()
        delay(200)
        assert(results[3] is JokeListState.Loaded)  { "Should progress to loaded with text LOADING" }
        assert((results[3] as JokeListState.Loaded).data.last().joke.contains("LOADING"))
        assert(results[4] is JokeListState.Loaded)

        viewModel.closeJokeList()
        delay(200)
        assert(results[5] is JokeListState.Empty) { "Shoudl return to empty but is ${results}" }
    }


    @Test
    @ExperimentalCoroutinesApi
    fun `List state returns temporary errors`() = runBlocking {

        coEvery { mockWebService.getRandomJokes(any()) } returns Result.failure(ApiException("Please try again"))

        val results = mutableListOf<JokeListState>()
        viewModel.uiState.onEach { results.add(it) }.launchIn(CoroutineScope(
            UnconfinedTestDispatcher()))

        viewModel.showJokeList()
        delay(1000)
        assert(results[0] is JokeListState.Empty) { "Should start in empty state" }
        assert(results[2] is JokeListState.Error) { "Should end in error is ${results[1]}" }
        val state=viewModel.uiState.value
        assert(state is JokeListState.Error)
        state as JokeListState.Error
        assert(state.message == "Please try again")
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `List state returns permanent errors`() = runBlocking {

        coEvery { mockWebService.getRandomJokes(any()) } returns Result.failure(ApiException("Unable to load jokes"))

        val results = mutableListOf<JokeListState>()
        viewModel.uiState.onEach { results.add(it) }.launchIn(CoroutineScope(
            UnconfinedTestDispatcher()))

        viewModel.showJokeList()
        delay(100)
        assert(results[0] is JokeListState.Empty) { "Should start in empty state" }
        assert(results[2] is JokeListState.Error) { "Should end in error" }
        val state=viewModel.uiState.value
        assert(state is JokeListState.Error)
        state as JokeListState.Error
        assert(state.message == "Unable to load jokes") { "should be 'Unable to load jokes' got '${state.message}'"}
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `Checkbox toggle works`() = runBlocking {
        val results = mutableListOf<Boolean>()
        viewModel.explicitToggleState.onEach { results.add(it) }.launchIn(CoroutineScope(
            UnconfinedTestDispatcher()))

        viewModel.toggleExplicit()
        viewModel.toggleExplicit()
        viewModel.toggleExplicit()

        assert(!results[0])
        assert(results[1])
        assert(!results[2])
        assert(results[3])
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `Opens input box for named joke`() = runBlocking {
        val results = mutableListOf<TextInputJokeState>()
        viewModel.textInputState.onEach { results.add(it) }.launchIn(CoroutineScope(
            UnconfinedTestDispatcher()))

        viewModel.askForTextInput()
        assert(results[0] is TextInputJokeState.Empty)
        assert(results[1] is TextInputJokeState.EnterInput)

    }

    @Test
    @ExperimentalCoroutinesApi
    fun `Named Joke tests input values correctly`() = runBlocking {

        coEvery { mockWebService.getNamedJoke(any(),any()) } returns Result.success(
            SingleJokeResponse("",goodJoke)
        )

        val results = mutableListOf<TextInputJokeState>()
        viewModel.textInputState.onEach { results.add(it) }.launchIn(CoroutineScope(
            UnconfinedTestDispatcher()))

        viewModel.askForTextInput()
        delay(50)
        viewModel.getNamedJoke("badString")
        delay(50)
        viewModel.askForTextInput()
        delay(50)
        viewModel.getNamedJoke("")
        delay(50)
        viewModel.askForTextInput()
        delay(50)
        viewModel.getNamedJoke("Good name")
        delay(50)
        viewModel.closeNamedJoke()
        delay(50)

        assert(results[0] is TextInputJokeState.Empty)
        assert(results[1] is TextInputJokeState.EnterInput)
        assert(results[2] is TextInputJokeState.InputError)
        assert(results[3] is TextInputJokeState.EnterInput)
        assert(results[4] is TextInputJokeState.InputError)
        assert(results[5] is TextInputJokeState.EnterInput)
        assert(results[6] is TextInputJokeState.Loaded)
        assert(results[7] is TextInputJokeState.Empty)
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `Can get single Joke`() = runBlocking {
        coEvery { mockWebService.getSingleJoke() } returns Result.success(
            SingleJokeResponse("",goodJoke)
        )

        val results = mutableListOf<JokeListState>()
        viewModel.singleJokeState.onEach {
            results.add(it)
        }.launchIn(CoroutineScope(UnconfinedTestDispatcher()))


        viewModel.getSingleJoke()
        delay(200)
        viewModel.closeSingleJoke()
        delay(200)

        assert(results[0] is JokeListState.Empty)
        assert(results[1] is JokeListState.Loaded)
        assert(results[2] is JokeListState.Empty)
    }
}
package uk.foocode.anddigitalchallenge.data.model

import com.google.gson.Gson
import org.junit.Before
import org.junit.Test

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 31/05/2022.
 */
class JokeListResponseTest {

    private val goodListResponse="""
        { "type": "success", "value": [ { "id": 235, "joke": "Chuck Norris once kicked a horse in the chin. Its descendants are known today as Giraffes.", "categories": [] }, { "id": 372, "joke": "Chuck Norris once rode a nine foot grizzly bear through an automatic car wash, instead of taking a shower.", "categories": [] }, { "id": 59, "joke": "Chuck Norris once roundhouse kicked someone so hard that his foot broke the speed of light, went back in time, and killed Amelia Earhart while she was flying over the Pacific Ocean.", "categories": [] } ]  }
    """.trimIndent()

    private val goodSingleResponse = """{ "type": "success", "value": { "id": 423, "joke": "Chuck Norris had to stop washing his clothes in the ocean. The tsunamis were killing people.", "categories": [] } }"""

    private lateinit var gson:Gson

    @Before
    fun setUp(){
        gson= Gson()
    }

    @Test
    fun `Can parse valid JSON`(){
        val response=gson.fromJson(goodListResponse.reader(),JokeListResponse::class.java)
        assert(response.type=="success"){ "Can't parse type field"}
        assert(!response.value.isNullOrEmpty()){ "Can't parse joke list"}
    }

    @Test
    fun `Can't parse bad JSON`(){
        try{
            gson.fromJson(goodSingleResponse.reader(),JokeListResponse::class.java)
            assert(false){ "Should fail for single response" }
        } catch (e:Exception){
            assert(true)
        }
    }
}
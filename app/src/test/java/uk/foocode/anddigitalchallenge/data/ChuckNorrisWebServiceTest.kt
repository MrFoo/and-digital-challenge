package uk.foocode.anddigitalchallenge.data

import io.mockk.coEvery
import io.mockk.mockkClass
import kotlinx.coroutines.runBlocking
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Before
import org.junit.Test
import retrofit2.Response
import uk.foocode.anddigitalchallenge.data.model.Joke
import uk.foocode.anddigitalchallenge.data.model.JokeListResponse
import uk.foocode.anddigitalchallenge.data.model.SingleJokeResponse
import java.util.concurrent.TimeoutException

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 01/06/2022.
 */
class ChuckNorrisWebServiceTest {

    private val mockkRetrofit = mockkClass(ChuckNorrisApi::class)

    private val goodResponse=JokeListResponse("success", listOf(
        Joke(1,"Joke body 1",listOf("a","b")),
        Joke(2,"Joke body 2", listOf("b","c"))
    ))

    private val goodFirstName="Testy"
    private val goodLastName="McTestington"

    private val goodSingleResponse = SingleJokeResponse("success",Joke(1,"Joke body 1",listOf("a","b")))
    private val goodNamedResponse = SingleJokeResponse("success",Joke(1,"Joke $goodFirstName body 1 $goodLastName",listOf("a","b")))

    private val GOOD_COUNT = 2
    private val PERMANENT_ERROR_COUNT = 404
    private val NETWORK_ERROR_COUNT = 501

    private lateinit var webService:ChuckNorrisWebService

    @Before
    fun setUp(){
        coEvery { mockkRetrofit.getMultipleJokes(GOOD_COUNT) } returns Response.success(goodResponse)
        coEvery {  mockkRetrofit.getMultipleJokes(PERMANENT_ERROR_COUNT) } returns Response.error(404,
            ResponseBody.create(MediaType.get("text/plain"),"Not found")
        )
        coEvery { mockkRetrofit.getMultipleJokes(NETWORK_ERROR_COUNT) } throws TimeoutException("Timeout!")
        coEvery { mockkRetrofit.getSingleJoke() } returns Response.success(goodSingleResponse)
        coEvery { mockkRetrofit.getSingleJoke(goodFirstName,goodFirstName) } returns Response.success(goodNamedResponse)
        webService= ChuckNorrisWebService(mockkRetrofit)
    }

    @Test
    fun `Can return successful request`() = runBlocking {
        val response=webService.getRandomJokes(GOOD_COUNT)
        assert(response.isSuccess) { "Response should be Success" }
        assert(!response.getOrNull()?.value.isNullOrEmpty()) { "Joke list should not be null" }
        assert(response.getOrNull()!!.value?.size == 2) { "Joke list should have 2 jokes" }
    }

    @Test
    fun `Returns failure for transient problems`() = runBlocking {
        val response=webService.getRandomJokes(NETWORK_ERROR_COUNT)
        assert(response.isFailure) { "Response should be a Temporary Error"}
        assert(response.exceptionOrNull()?.message == "Timeout!" ) { "Exception message should be in reason" }
    }

    @Test
    fun `Returns Failure for permanent problems`()= runBlocking {
        val response=webService.getRandomJokes(PERMANENT_ERROR_COUNT)
        assert(response.isFailure) { "Response should be a Failure"}
        assert(response.exceptionOrNull()?.message == "Not found" ) { "Exception message should be in reason" }
    }

    @Test
    fun `Returns single Joke`():Unit= runBlocking {
        val response=webService.getSingleJoke()
        assert(response.isSuccess)
        response.onSuccess {
            assert(it == goodSingleResponse)
        }
        response.onFailure {
            assert(false) { "should not happen" }
        }
    }

    @Test
    fun `Returns named Joke`():Unit= runBlocking {
        val response=webService.getNamedJoke(goodFirstName,goodFirstName)
        assert(response.isSuccess)
        response.onSuccess {
            assert(it == goodNamedResponse)
        }
        response.onFailure {
            assert(false) { "should not happen" }
        }
    }

}
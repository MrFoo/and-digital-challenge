package uk.foocode.anddigitalchallenge.data

import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import uk.foocode.anddigitalchallenge.data.model.JokeListResponse

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 31/05/2022.
 */
class ChuckNorrisApiTest {

    private lateinit var client: ChuckNorrisApi

    @Before
    fun setUp(){
        client=Retrofit.Builder().baseUrl("http://api.icndb.com/jokes/").addConverterFactory(GsonConverterFactory.create()).build().create(ChuckNorrisApi::class.java)
    }

    @Test
    fun `Can get simple joke list`() = runBlocking {
        val response=client.getMultipleJokes(2)
        testResponse(response)
    }

    @Test
    fun `Can request 1 joke from list view`() = runBlocking {
        val response=client.getMultipleJokes(1)
        testResponse(response)
    }

    @Test
    fun `Can get single Joke`():Unit = runBlocking {
        val response=client.getSingleJoke()
        assert(response.isSuccessful)
        val body=response.body()
        assert(body != null)
        body?.let {
            assert(body.value.joke.isNotBlank())
        }
    }

    @Test
    fun `Can get jokes with custom name`():Unit = runBlocking {
        val response = client.getMultipleJokes(20,"Testy","McTestington")
        assert(response.isSuccessful)
        val body=response.body()
        assert(body != null)
        body?.let{
            assert(body.value?.isNotEmpty() == true)
            // can't check for "Testy McTestington" as some jokes only use part of the name.
            // Some jokes don't even use the name, so as long as some contain it, it mush be sort of working?
            assert(body.value?.any{ it.joke.contains("Testy") || it.joke.contains("McTestington") } == true)
        }
    }

    @Test
    fun `Can request 0 jokes but get empty list`() = runBlocking {
        val response=client.getMultipleJokes(0)
        testResponse(response,false)
    }

    private fun testResponse(response:Response<JokeListResponse>, testNotEmpty:Boolean=true){
        assert(response.isSuccessful)
        assert(response.body() != null) { "Could not parse data correctly" }
        assert(response.body()?.type == "success") { "Response type is not success" }
        if(testNotEmpty) {
            assert(!response.body()?.value.isNullOrEmpty()) { "Response value is empty or null" }
        } else {
            assert(response.body()?.value?.isEmpty() == true) { "Response value is null not empty"}
        }
    }

}